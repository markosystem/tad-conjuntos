<?php
/**
 * Created by PhpStorm.
 * User: Marcos Batista
 * Date: 12/02/2015
 * Time: 12:21
 */
require_once "tad.php";
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Trabalho desenvolvido em PHP sobre Conjuntos e SobConjuntos">
    <meta name="keywords" content="Conjuntos, Subconjuntos, PHP">
    <meta name="author" content="Marcos Batista">
    <title>Trabalho de Matemática Discreta</title>
</head>
<body>
    <?php
        //criando instancia do conjuno A
        $A = new tad();

        //criando elementos para o conjunto A
        $Conjunto1[] = 4;
        $Conjunto1[] = 3;
        $Conjunto1[] = 5;

        //inserindo elementos no conjunto A
        $A->inserirConjunto($Conjunto1);

        $A->imprimir("Imprindo o conjunto A");

        //inserindo um elemento no conjunto A
        $A->inserirElemento(1);

        $A->imprimir("Imprindo o conjunto A depois de inserir um elemento (1)");

        //criando elementos para um elemento do conjunto A
        $Conjunto2[] = 9;
        $Conjunto2[] = 10;
        $Conjunto2[] = 55;
        $Conjunto2[] = 11;

        //inserindo um conjunto dentro do conjunto A
        $A->inserirConjuntoOnConjunto($Conjunto2);

        $A->imprimir("Conjunto X dentro do Conjunto A");

        //criando instancia do conjuno A
        $B = new tad();

        //criando elementos para um elemento do conjunto B
        $Conjunto3[] = 9;
        $Conjunto3[] = 2;
        $Conjunto3[] = 4;
        $Conjunto3[] = 1;

        //inserindo elementos no conjunto B
        $B->inserirConjunto($Conjunto3);

        $B->imprimir("Imprindo o conjunto B");

		 //criando elementos para comparação de um elemento do conjunto A
        $Conjunto5[] = 11;
        $Conjunto5[] = 55;
        $Conjunto5[] = 10;
        $Conjunto5[] = 9;
		
		$A->imprimir("Imprimindo um para verificação o Conjunto5", $Conjunto5);

        $A->imprimir("Verificar se elemento(Conjunto5) esta no Conjunto A", $A->verificarElementoOnConjunto($Conjunto5));
			
        $A->imprimir("Verificar se elemento(55) esta no Conjunto A", $A->verificarElementoOnConjunto(55));

        $B->imprimir("Verificar se elemento(6) esta no Conjunto B", $B->verificarElementoOnConjunto(6));

        //criando instancia do conjuno A
        $C = new tad();

        //criando elementos para um elemento do conjunto C
        $Conjunto4[] = 1;
        $Conjunto4[] = 4;
        $Conjunto4[] = 2;
        $Conjunto4[] = 9;
        $Conjunto4[] = 20;

        //inserindo elementos no conjunto C
        $C->inserirConjunto($Conjunto4);

        $C->imprimir("Imprindo o conjunto C");

        $C->imprimir("Verificar se Conjunto C e SubConjunto do Conjunto B",$C->verificarConjuntoOnSubConjuntoORIgual($B->Conjunto()));
    ?>
</body>
</html>