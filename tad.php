<?php
/**
 * Created by PhpStorm.
 * User: Marcos Batista
 * Date: 12/02/2015
 * Time: 12:20
 */

class tad{
    //variavel global sem tipo
    var $conjunto;
    //retorna o conjunto da instancia
    function Conjunto(){
        return $this->conjunto;
    }
    //insere um novo elemento na instancia
    function inserirElemento($elemento){
        $this->conjunto[] = $elemento;
    }
    //insere um novo conjunto
    function inserirConjunto($conjunto){
        $this->conjunto = $conjunto;
    }
    //insere um novo conjunto em um elemento do conjunto instanciado
    function inserirConjuntoOnConjunto($conjunto){
        $this->conjunto[] = $conjunto;
    }
	
    //verifica se um elemento está dentro de um conjunto instanciado
    function verificarElementoOnConjunto($elemento){
        foreach($this->Conjunto() as $k => $elementoConjunto){
			if(!is_array($elementoConjunto)){
				if(!is_array($elemento)){
					if($elemento == $elementoConjunto){
						return "Sim";
					}
				}
			}elseif(is_array($elemento)){
				$result = array_diff($elementoConjunto, $elemento);
                if(empty($result)){
                    return "Sim";
                }
			}
        }
        return "Nao";
    }
    //verificar se um conjunto é igual a outro ou se o conjunto é subconjunto proprio do instanciado
    function verificarConjuntoOnSubConjuntoORIgual($conjunto2){
        $result1 = array_diff($this->Conjunto(),$conjunto2);
        $result2 = array_diff($conjunto2,$this->Conjunto());
        if(empty($result1) && empty($result2)){
            return "Os Conjuntos sao iguais!";
        }else{
            if(!empty($result2)){
                return "O Conjunto 1 e SubConjunto Proprio do Conjunto 2";
            }else{
                return "O Conjunto 1 Nao e SubConjunto do Conjunto 2";
            }
        }
    }
    //imprimir valores instanciados ou por parametros
    function imprimir($msg, $valor = null){
        print_r("<pre>");
        print_r($msg."\n");
        print_r($valor ? $valor:$this->Conjunto());
        print_r("</pre>");
    }
}